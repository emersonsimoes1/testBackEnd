PHP project
===========


Projeto Sistema PC4 Controle de Alunos
---------------------------------

This project was generated with: 

 - **Laravel Framework 5.6.40**
 - **PHP 7.2**

**Sistema de solicitação e requerimento de pensão, ebertura de processo e acompanhamento digital**

 - Branch Main - inicial

 - Branch release/v.0.0.7 - versão atual em desenvolvimento.


**Version 0.0.7** - [Changelog](changelog.md)

**Version 0.0.1**


Leia-me.

## Instalação

Git clone


 ## API's

**Lista alunos, turmas e Escolas**
 - http://localhost:8000/api/alunos 
 - http://localhost:8000/api/turmas 
 - http://localhost:8000/api/escolas 

 Obs: localhost e porta variaveis.


Instalar conversor de PDF
--------------------------
**Acoes**

 - criar Base de dados;

controle_alunos

Depois:

```
php artisan migrate
```

Em seguida:


```
php artisan db:seed
```

rodar o codigo:

```
php artisan serve
```

Autor:
----------

- Emerson Simões - Desenvolvedor
