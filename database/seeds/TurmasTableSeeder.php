<?php

use Illuminate\Database\Seeder;
use App\ModelTurma;

class TurmasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ModelTurma::class, 26)->create();
    }
}
