<?php

use Illuminate\Database\Seeder;
use App\ModelAluno;

class AlunosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ModelAluno::class, 650)->create();
        // $faker = \Faker\Factory::create('pt-BR');
        // foreach(range(1,650) as $index){

        //     // Returns always random genders according to the name, inclusive mixed !!
        //     $gender = $faker->randomElement($array = array('male','female','mixed'));

        //     DB::table('alunos')->insert([
        //         'nome' => $faker->name($gender), 
        //         // 'telefone' => $faker->phonenumber('## ########'),             
        //         'telefone' => $faker->numerify('##########'),             
        //         'email' => $faker->unique()->safeEmail,
        //         'dtnascimento' => $faker->date($format = 'Y-m-d', $max = 'now'),
        //         'genero' => $gender,
        //         'created_at' => now(),
        //         'updated_at' => now(),
        //     ]);
        // }
    }
}
