<?php

use Faker\Generator as Faker;

$factory->define(App\ModelEscola::class, function (Faker $faker) {
    
    return [
        'escola' => $faker->company,          
        'cep' => $faker->postcode,             
        'logradouro' => $faker->streetAddress,             
        'numero' => $faker->numerify('####'),           
        'bairro' => $faker->lastName,           
        'cidade' => $faker->city,             
        'uf' => $faker->state,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
