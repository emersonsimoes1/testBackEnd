<?php

use Faker\Generator as Faker;

$factory->define(App\ModelAluno::class, function (Faker $faker) {

    $gender = $faker->randomElement($array = array('M','F','-'));

    return [
        'nome' => $faker->name($gender), 
        // 'telefone' => $faker->phonenumber('## ########'),             
        'telefone' => $faker->numerify('##########'),             
        'email' => $faker->unique()->safeEmail,
        'dtnascimento' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'genero' => $gender,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
