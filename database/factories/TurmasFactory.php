<?php

use Faker\Generator as Faker;

$factory->define(App\ModelTurma::class, function (Faker $faker) {

    $nivel = $faker->randomElement($array = array('Médio','Fundamental'));
    if($nivel == 'Médio'){
        $serie = $faker->randomElement($array = array('1º','2º', '3º'));
    }else{
        $serie = $faker->randomElement($array = array('1ª','2ª', '3ª', '4ª', '5ª', '6ª', '7ª', '8ª', '9º'));
    }
    $turno = $faker->randomElement($array = array('Manhã','Tarde'));
    $escola  = $faker->randomElement($array = array(1, 2, 3, 4, 5, 6));

    return [
        'ano' => $faker->date($format = 'Y', $max = 'now'),     
        'nivel' => $nivel,             
        'serie' => $serie,             
        'turno' => $turno,           
        'escola_id' => $escola, 
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
