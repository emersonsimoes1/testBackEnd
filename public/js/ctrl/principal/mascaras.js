<!--Inicio mascaras-->
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function($){
            $("#data").mask("99/99/9999");
            $("#tempo").mask("00:00:00");
            $("#data_tempo").mask("99/99/9999 00:00:00");
            $("#telefone").mask("(99) 9999-9999");
            $("#telefone2").mask("(99) 9999-9999");
            $("#celular").mask("(99) 99999-9999");
            $("#celular2").mask("(99) 99999-9999");
            $("#cpf").mask("999.999.999-99");
            $("#buscarcpf").mask("999.999.999-99");
            $("#cep").mask("99999-999");
            $("#cnpj").mask("99.999.999/9999-99");
            $("#placa").mask("aaa - 9999");
            $("#dinheiro").mask("000.000.000.000.000,00");
            $("#dinheiro2").mask("#.##0,00");
            $('.dinheiro').mask('#.##0,00', {reverse: true});
            $('.valor').mask('###0.00', {reverse: true}); //padrão americano
            $('.conta').mask('###########-#', {reverse:true});
            $("#dataDeCriacao").mask('99/99/9999');
            $(".numeroCalculo").mask('####/9999');
        });
    </script>
    <!--Fim marcaras-->