<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelAlunoTurma extends Model
{
    protected $table = 'alunos_turmas';

    protected $fillable = [
        'aluno_id', 'turma_id'
    ];

    public function aluno()
    {
        return $this->belongsTo(ModelAluno::class, 'aluno_id', 'id');
    }

    public function turma()
    {
        return $this->belongsTo(ModelTurma::class, 'turma_id', 'id');
    }
}
