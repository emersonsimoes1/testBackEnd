<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FiltrarEscolasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function prepareForValidation() {

        $this->merge([

            'escola' => trim(strip_tags($this->escola)), //strtoupper(trim(strip_tags($this->nome))), deixando a pesquina em maisculo
            'bairro' => trim(strip_tags($this->bairro)),
            'cidade' => trim(strip_tags($this->cidade)),
            'uf' => trim(strip_tags($this->uf))
        ]);
    }
}
