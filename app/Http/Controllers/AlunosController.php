<?php

namespace App\Http\Controllers;

use App\Http\Requests\FiltrarAlunosRequest;
use App\ModelAluno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DateTime;
use Illuminate\Support\Facades\DB;
use App\ModelTurma;
use App\ModelAlunoTurma;

class AlunosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('alunos.list')
        ->with('alunos', ModelAluno::orderby('nome')->paginate(15))
        ->with('i', ($request->input('page', 1) - 1) * 15);
    }

    /**
     * Filtro de alunos.
     * @parametro nome, telefone, email, data de nascimento e gênero
     *
     * @return \Illuminate\Http\Response
     */
    public function filtrarAlunos(FiltrarAlunosRequest $request)
    {
        $filtros = $request->except('_token');
        Log::info($filtros);
        
        $alunos = DB::table('alunos')
        ->select('id','nome', 'telefone', 'email', 'dtnascimento', 'genero')
        ->where(function ($query) use ($filtros) {

            if($filtros['nome'] != ''){
                $query->where('nome', 'like', '%'.$filtros['nome'].'%');
            }

            if($filtros['telefone'] != ''){
                $query->where('telefone', 'like', '%'.$filtros['telefone'].'%');
            }

            if($filtros['email'] != ''){
                $query->where('email', 'like', '%'.$filtros['email'].'%');
            }

            if($filtros['dtnascimento'] != ''){

                $data = explode('/', $filtros['dtnascimento']);
                $data = $data[2] . '-' . $data[1] . '-' . $data[0];
                $data = new DateTime($data);
                $filtros['dtnascimento'] = date_format($data, 'Y-m-d');

                $query->whereDate('dtnascimento', $filtros['dtnascimento']);
            }

            if($filtros['genero'] != ''){
                $query->where('genero', 'like', '%'.$filtros['genero'].'%');
            }
        })
        ->orderBy('nome', 'ASC')
        ->paginate(15);

        if(!count($alunos)){

            $pesquisaRetornouVazio = true;

            return view('alunos.list')
                ->with(['alunos' => $alunos, 'pesquisaRetornouVazio' => $pesquisaRetornouVazio]);
        }

        return view('alunos.list')
        ->with(['alunos' => $alunos, 'filtros' => $filtros])
        ->with('i', ($request->input('page', 1) - 1) * 15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $turmas = ModelTurma::orderBy('ano', 'DESC')->get();
        return view('alunos.form')
        ->with('turmas', $turmas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ModelAluno::create($request->all());

        $aluno = new ModelAluno;
        $aluno->nome = $request->nome;
        $aluno->telefone = $request->telefone;
        $aluno->email = $request->email;
        $aluno->dtnascimento = $request->dtnascimento;
        $aluno->genero = $request->genero;
        $aluno->save();
        $idAluno = $aluno->id;

        $alunoTurma = new ModelAlunoTurma;
        $alunoTurma->aluno_id = $idAluno;
        $alunoTurma->turma_id = $request->turma;
        $alunoTurma->save();



        $msg = "O Aluno ". $request->input('nome'). " foi cadastrado com sucesso!";

        return redirect()
        ->action('AlunosController@index')
        ->with('statusSucesso', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelAluno  $modelAluno
     * @return \Illuminate\Http\Response
     */
    public function show(ModelAluno $modelAluno)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelAluno  $modelAluno
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ModelAluno $modelAluno)
    {
        $aluno = ModelAluno::find($request->input('aluno_id'));
        $turmas = ModelTurma::orderBy('ano', 'DESC')->get();
        $alunoTurma = ModelAlunoTurma::where('aluno_id', $aluno->id)->first();

        return view('alunos.form')
        ->with('turmas', $turmas)
        ->with('aluno', $aluno)
        ->with('alunoturma', $alunoTurma);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelAluno  $modelAluno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelAluno $modelAluno)
    {
        $id = $request->input('id');
        $updateAluno = ModelAluno::find($id);
        $updateAluno->nome = $request->nome;
        $updateAluno->telefone = $request->telefone;
        $updateAluno->email = $request->email;
        $updateAluno->dtnascimento = $request->dtnascimento;
        $updateAluno->genero = $request->genero;
        $updateAluno->save();
        $idAluno = $updateAluno->id;

        $alunoTurma = ModelAlunoTurma::where('aluno_id', $request->input('id'))->where('turma_id', $request->turma)->orderby('id', 'DESC')->first();
        $alunoTurma->aluno_id = $request->input('id');
        $alunoTurma->turma_id = $request->turma;
        $alunoTurma->save();

        $msg = "O aluno ". $request->input('nome'). " foi atualizado com sucesso!";

        return redirect()
        ->action('AlunosController@index')
        ->with('statusSucesso', $msg);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelAluno  $modelAluno
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelAluno $modelAluno)
    {
        //
    }
}
