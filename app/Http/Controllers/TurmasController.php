<?php

namespace App\Http\Controllers;

use App\ModelTurma;
use Illuminate\Http\Request;
use App\Http\Requests\FiltrarTurmasRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\ModelEscola;

class TurmasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('turmas.list')
        ->with('turmas', ModelTurma::orderby('serie')->paginate(8))
        ->with('i', ($request->input('page', 1) - 1) * 8);
    }

    public function filtrarTurmas(FiltrarTurmasRequest $request)
    {
        $filtros = $request->except('_token');
        Log::info($filtros);
        
        $turmas = DB::table('turmas')
        ->join('escolas as e', 'e.id', '=', 'turmas.escola_id')
        ->select('turmas.id','ano', 'nivel', 'serie', 'turno', 'escola_id', 'escola', 'turmas.updated_at')
        ->where(function ($query) use ($filtros) {

            if($filtros['ano'] != ''){
                $query->where('ano', 'like', '%'.$filtros['ano'].'%');
            }

            if($filtros['nivel'] != ''){
                $query->where('nivel', 'like', '%'.$filtros['nivel'].'%');
            }

            if($filtros['serie'] != ''){
                $query->where('serie', 'like', '%'.$filtros['serie'].'%');
            }

            if($filtros['turno'] != ''){
                $query->where('turno', 'like', '%'.$filtros['turno'].'%');
            }

            if($filtros['escola'] != ''){
                $query->where('escola', 'like', '%'.$filtros['escola'].'%');
            }
        })
        ->orderBy('serie', 'ASC')
        ->paginate(8);

        if(!count($turmas)){

            $pesquisaRetornouVazio = true;

            return view('turmas.list')
                ->with(['turmas' => $turmas, 'pesquisaRetornouVazio' => $pesquisaRetornouVazio]);
        }

        return view('turmas.list')
        ->with(['turmas' => $turmas, 'filtros' => $filtros])
        ->with('i', ($request->input('page', 1) - 1) * 8);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $escolas = ModelEscola::orderBy('escola', 'ASC')->get();
        return view('turmas.form')
        ->with('escolas', $escolas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ModelTurma::create($request->all());

        $msg = "Turma do ". $request->input('nivel'). " cadastrada com sucesso!";

        return redirect()
        ->action('TurmasController@index')
        ->with('statusSucesso', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelTurma  $modelTurma
     * @return \Illuminate\Http\Response
     */
    public function show(ModelTurma $modelTurma)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelTurma  $modelTurma
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ModelTurma $modelTurma)
    {

        $turma = ModelTurma::find($request->input('turma_id'));
        $escolas = ModelEscola::orderBy('escola', 'ASC')->get();

        return view('turmas.form')
        ->with('turma', $turma)
        ->with('escolas', $escolas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelTurma  $modelTurma
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelTurma $modelTurma)
    {
        $id = $request->input('id');
        $turma = ModelTurma::find($id);

        $dados = $request->all();
        $turma->fill($dados)->save();
        $turma->save();

        $msg = "Turma do ". $request->input('nivel'). " foi atualizada com sucesso!";

        return redirect()
        ->action('TurmasController@index')
        ->with('statusSucesso', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelTurma  $modelTurma
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ModelTurma $modelTurma)
    {
        $id = $request->input('id');
        $turma = ModelTurma::find($id);
        $turma->delete();


        $msg = "Turma do ". $request->input('nivel'). " foi deletada com sucesso!";

        return redirect()
        ->action('TurmasController@index')
        ->with('statusSucesso', $msg);
    }
}
