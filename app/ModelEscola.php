<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelEscola extends Model
{
    protected $table = 'escolas';

    protected $fillable = [
        'escola', 'cep', 'logradouro', 'numero', 'complemento', 'bairro', 'cidade', 'uf'
    ];

    public function turmas()
    {
        return $this->hasOne(ModelTurmas::class, 'escola_id','id');
    }
}
