<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelAluno extends Model
{
    protected $table = 'alunos';

    protected $fillable = [
        'nome', 'telefone', 'email', 'dtnascimento', 'genero'
    ];

    public function alunoturma()
    {
        return $this->hasMany(ModelAlunoTurma::class, 'aluno_id','id');
    }
}
