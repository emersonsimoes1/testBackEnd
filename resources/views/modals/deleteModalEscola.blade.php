<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="modalExcluirEscolas{{$e->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Excluir item</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <Form action="{{route('delete.escola')}}" method="get">
          <input type="hidden" name="id" value="{{$e->id}}" />
            {{ csrf_field() }}
            <div class="modal-body">
            Você quer realmente excluir o item?
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger buttonleft">Sim, Quero Excluir.</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>
            
            </div>
        </form>
      </div>
    </div>
  </div>