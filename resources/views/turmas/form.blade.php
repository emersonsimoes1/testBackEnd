
@extends('layouts.principal')

@section('content')

<div class="container">
  <div class="card-header textoBold"><h4>TURMAS <span><i> -  @if(isset($turma)) editar turma @else nova turma @endif</i></span></h4></div>
  <hr>
  @if(isset($turma))
    <form action="{{route('update.turma')}}" method="post" class="row g-3">
    <input type="hidden" name="id" value="{{$turma->id}}" />
  @else
    <form action="{{route('store.turma')}}" method="post" class="row g-3">
  @endif   
  {{ csrf_field() }} 
      <div class="col-md-2">
        <label for="validationDefault01" class="form-label">Ano</label>
        <input type="text" name="ano" class="form-control" id="validationDefault01" required value="{{$turma->ano ?? ''}}">
      </div>
      <div class="col-md-3">
        <label for="validationDefault02" class="form-label">Nível</label>
        <select class="form-control" name="nivel" aria-label="Default select example">
            @if(!isset($turma))
                <option selected>Escolha...</option>
                <option value="Fundamental">Fundamental</option>
                <option value="Médio">Médio</option>
            @else
                @if($turma->nivel == 'Fundamental')
                    <option value="Fundamental" selected>Fundamental</option>
                    <option value="Médio">Médio</option>
                @elseif($turma->nivel == 'Médio')
                    <option value="Médio" selected>Médio</option>
                    <option value="Fundamental">Fundamental</option>
                @endif
            @endif
        </select>
        {{-- <input type="text" name="nivel" class="form-control" id="validationDefault02" value="Otto" required> --}}
      </div>
      <div class="col-md-2">
        <label for="validationDefaultUsername" class="form-label">Série</label>
        <div class="input-group">
          {{-- <span class="input-group-text" id="inputGroupPrepend2">@</span> --}}
          <input type="text" name="serie" class="form-control" id="validationDefaultUsername"  aria-describedby="inputGroupPrepend2" required value="{{$turma->serie ?? ''}}">
        </div>
      </div>
      <div class="col-md-2">
        <label for="validationDefault03" class="form-label">Turno</label>
        <select class="form-control" name="turno" aria-label="Default select example">
            @if(!isset($turma))
                <option value="Manhã">Manhã</option>
                <option value="Tarde">Tarde</option>
                <option value="Noite">Noite</option>
            @else
                @if($turma->turno == 'Manhã')
                    <option value="Manhã" selected>Manhã</option>   
                    <option value="Tarde">Tarde</option>
                    <option value="Noite">Noite</option>
                @elseif($turma->turno == 'Tarde')
                    <option value="Tarde" selected>Tarde</option>
                    <option value="Noite">Noite</option>
                    <option value="Manhã">Manhã</option>
                @elseif($turma->turno == 'Noite')
                    <option value="Noite" selected>Noite</option>
                    <option value="Manhã">Manhã</option>
                    <option value="Tarde">Tarde</option>
                @endif
            @endif
        </select>
        {{-- <input type="text" name="Turno" class="form-control" id="validationDefault03" required> --}}
      </div>
      <div class="col-md-3">
        <label for="" class="form-label">Escola</label>
        <select class="form-control" name="escola_id" aria-label="Default select example">
          @foreach($escolas as $e)
          <option value='{{$e->id}}' @if(isset($turma)) @if($e->id == $turma->escola_id) selected @endif @endif>{{$e->escola}}</option>
          @endforeach
        </select>
      </div>
      <div class="col-12 buttonRight">
        <hr>
        <button class="btn btn-primary" type="submit">@if(!isset($turma))Cadastrar @else Atualizar @endif</button>
      </div>
    </form>

  </div>
</div>

@endsection