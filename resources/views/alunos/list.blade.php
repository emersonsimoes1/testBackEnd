@extends('layouts.principal')

@section('content')

<div class="container">
     <!--Filtro-->
     <form action="{{route('filtrarAlunos')}}" class="" id="filtrarAlunos" method="get">
        <div class="form-row">
            <div class="form-group col-md-4 formGroupMeio" id="formGroupRequerente">
                <label for="nome">Nome do Aluno</label>
                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome ou parte dele"  @if(isset($filtros)) value="{{$filtros['nome']}} "@endif>
            </div>
            <div class="form-group col-md-2 formGroupMeio" id="formGroupRequerimento">
                <label for="telefone">Telefone</label>
                <input type="text" class="form-control" id="telefone" name="telefone" placeholder="(99)9999-9999"   @if(isset($filtros)) value="{{$filtros['telefone']}}" @endif>
            </div>
            <div class="form-group col-md-3 formGroupMeio" id="formGroupServidor">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="exemplo@exemplo.com"   @if(isset($filtros)) value="{{$filtros['email']}} " @endif>
            </div>
            <div class="form-group col-md-2 formGroupDir {{$errors->has('dtnascimento') ? 'has-error': ''}}" id="formGroupData">
                <label for="Data">Data Nascimento</label>
                <input type="text" class="form-control" id="dtnascimento" placeholder="__/__/____" name="dtnascimento"  @if(isset($filtros)) value="{{$filtros['dtnascimento']}} " @endif>
                @if($errors->has('dtnascimento'))
                    <div class="">
                        <span style="color: red">{{$errors->first('dtnascimento')}}</span>
                    </div>
                @endif
            </div>
            <div class="form-group col-md-1 formGroupMeio" id="formGroupServidor">
                <label for="genero">Gênero</label>
                <input type="text" class="form-control" id="genero" name="genero" placeholder="M ou F"   @if(isset($filtros)) value="{{$filtros['genero']}} " @endif>
            </div>
        </div>
        <div class="buttonRight">
            <button class="btn btn-primary">Pesquisar</button>
            <a href="/alunos" class="btn btn-secondary">Limpar</a>
        </div>
    </form>
    <!--Fim Filtro-->
    @if(session('statusFalha'))
        <div class="alert alert-danger">
            {{ session('statusFalha') }}
        </div>
    @elseif (session('statusSucesso'))
        <div class="alert alert-success">
            {{ session('statusSucesso') }}
        </div>
    @endif
    @if(!count($alunos) && isset($pesquisaRetornouVazio))
        <div class="alert alert-danger">
            Nenhum aluno encontrado com os parâmetros passados na pesquisa.
        </div>
    @else
    <div class="card-header textoBold">ALUNOS</div>
        <div class="table-responsive">
            <table class="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Nascimento</th>
                    <th scope="col">Gênero</th>
                    <th scope="col">
                        <div class=".col-lg-12 buttonRight">
                            <a href="{{route('new.aluno')}}" class="btn btn-primary btn-sm">Add Novo Aluno</a>
                            </div>
                    </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($alunos as $a)
                    <tr class="table-sem-quebra">
                        <th scope="row">{{++$i}}</th>
                        <td>{{$a->nome}}</td>
                        <td>{{$a->telefone ?? ''}}</td>
                        <td>{{$a->email}}</td>
                        <td>{{ucfirst(date("d/m/Y", strtotime($a->dtnascimento)))}}</td>
                        <td>{{$a->genero ?? ''}}</td>
                        <td>
                            <div class=".col-lg-12 buttonRight">
                            <a href="{{route('edit.aluno', ['aluno_id'=>$a->id])}}" class="btn btn-outline-primary btn-sm">Editar</a>
                            <a href="" class="btn btn-outline-danger btn-sm">Ecluir</a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if(isset($filtros))
                {{$alunos->appends($filtros)->links()}}
            @else
                {{$alunos->links()}}
            @endif
        </div>
    </div>
    @endif
</div>
<script src="/js/ctrl/alunos/pesquisa.js"></script>

@endsection