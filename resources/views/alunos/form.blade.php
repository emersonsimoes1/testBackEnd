
@extends('layouts.principal')

@section('content')

<div class="container">
  <div class="card-header textoBold"><h4>ALUNOS <span><i> -  @if(isset($aluno)) editar aluno @else novo aluno @endif</i></span></h4></div>
  <hr>
  @if(isset($aluno))
    <form action="{{route('update.aluno')}}" method="post" class="row g-3">
    <input type="hidden" name="id" value="{{$aluno->id}}" />
  @else
    <form action="{{route('store.aluno')}}" method="post" class="row g-3">
  @endif   
  {{ csrf_field() }} 
      <div class="col-md-8">
        <label for="validationDefault01" class="form-label">Nome</label>
        <input type="text" name="nome" class="form-control" id="validationDefault01" required value="{{$aluno->nome ?? old('nome')}}">
      </div>
      <div class="col-md-4">
        <label for="validationDefault01" class="form-label">Telefone</label>
        <input type="text" id="telefone" name="telefone" class="form-control" id="validationDefault01" required value="{{$aluno->telefone ?? old('telefone')}}">
      </div>
      <div class="col-md-12"><hr></div>
      <div class="col-md-6">
        <label for="validationDefault01" class="form-label">E-mail</label>
        <input type="text" name="email" class="form-control" id="validationDefault01" required value="{{$aluno->email ?? old('email')}}">
      </div>
      <div class="col-md-3">
        <label for="validationDefault01" class="form-label">Data Nascimento</label>
        <input type="date" name="dtnascimento" class="form-control" id="validationDefault01" required value="{{$aluno->dtnascimento ?? old('dtnascimento')}}">
      </div>
      <div class="col-md-3">
        <label for="validationDefault02" class="form-label">Gênero</label>
        <select class="form-control" name="genero" aria-label="Default select example">
            @if(!isset($aluno))
                <option value="-">Não informar</option>
                <option value="M">Masculino</option>
                <option value="F">Feminino</option>
            @else
                @if($aluno->genero == 'M')
                    <option value="-">Não informar</option>
                    <option value="M" selected>Masculino</option>
                    <option value="F">Feminino</option>
                @elseif($aluno->genero == 'F')
                    <option value="-">Não informar</option>
                    <option value="M">Masculino</option>
                    <option value="F" selected>Feminino</option>
                @else
                    <option value="-" selected>Não informar</option>
                    <option value="M">Masculino</option>
                    <option value="F">Feminino</option>
                @endif
            @endif
        </select>
      </div>
      <div class="col-md-6">
        <label for="validationDefault02" class="form-label">Turmas</label>
        <select class="form-control" name="turma" aria-label="Default select example">
            @foreach($turmas as $t)
                <option value="{{$t->id}}"  @if(isset($aluno)) @if($t->id == $alunoturma->turma_id) selected @endif @endif>{{$t->ano}} - {{$t->serie}} - {{$t->nivel}} - {{$t->turno}} - {{$t->escola->escola}}</option>
            @endforeach
        </select>
      </div>
      
      <div class="col-12 buttonRight">
        <hr>
        <button class="btn btn-primary" type="submit">@if(!isset($aluno))Cadastrar @else Atualizar @endif</button>
      </div>
    </form>

  </div>
</div>

@endsection