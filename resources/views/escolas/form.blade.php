
@extends('layouts.principal')

@section('content')

<div class="container">
  <div class="card-header textoBold"><h4>ESCOLAS <span><i> -  @if(isset($escola)) editar escola @else nova escola @endif</i></span></h4></div>
  <hr>
    @if(isset($escola))
        <form action="{{route('update.escola')}}" method="post" class="row g-3">
            <input type="hidden" name="id" value="{{$escola->id}}" />
    @else
        <form action="{{route('store.escola')}}" method="post" class="row g-3">
    @endif   
        {{ csrf_field() }} 

        <div class="col-md-12">
            <label for="validationDefault01" class="form-label">Nome da Escola</label>
            <input type="text" name="escola" class="form-control" id="validationDefault01" required value="{{$escola->escola ?? old('escola')}}"><hr>
        </div>
        <div class="col-md-3">
            <label for="validationDefault01" class="form-label">CEP</label>
            <input type="text" id="cep" name="cep" class="form-control" id="validationDefault01" required value="{{$escola->cep ?? old('cep')}}">
        </div>
        <div class="col-md-6">
            <label for="validationDefault01" class="form-label">Endereço</label>
            <input type="text" id="endereco" name="logradouro" class="form-control" id="validationDefault01" required value="{{$escola->logradouro ?? old('logradouro')}}">
        </div>
        <div class="col-md-3">
            <label for="validationDefault01" class="form-label">Nº</label>
            <input type="text" id="numero" name="numero" class="form-control" id="validationDefault01" required value="{{$escola->numero ?? old('numero')}}">
        </div>
        <div class="col-md-12"><hr></div>
        <div class="col-md-5">
            <label for="validationDefault01" class="form-label">Complemento</label>
            <input type="text" id="complemento" name="complemento" class="form-control" id="validationDefault01" value="{{$escola->complemento ?? old('complemento')}}">
        </div>
        <div class="col-md-3">
            <label for="validationDefault01" class="form-label">Bairro</label>
            <input type="text" id="bairro" name="bairro" class="form-control" id="validationDefault01" required value="{{$escola->bairro ?? old('bairro')}}">
        </div>
        <div class="col-md-3">
            <label for="validationDefault01" class="form-label">Cidade</label>
            <input type="text" id="cidade" name="cidade" class="form-control" id="validationDefault01" required value="{{$escola->cidade ?? old('cidade')}}">
        </div>
        <div class="col-md-1">
            <label for="validationDefault01" class="form-label">UF</label>
            <input type="text" id="estado" name="uf" class="form-control" id="validationDefault01" required value="{{$escola->uf ?? old('uf')}}">
        </div>
        <div class="col-12 buttonRight">
            <hr>
            <button class="btn btn-primary" type="submit">@if(!isset($escola))Cadastrar @else Atualizar @endif</button>
        </div>
        </form>

  </div>
</div>

@endsection